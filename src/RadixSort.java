import java.util.ArrayList;

public class RadixSort {
	static int base = 10;
	
	/**
	 * This variable contains snapshots of the bucketlist that shall be taken at the end of each sorting 
	 * iteration (= each time after all elements to be sorted have been assigned to a bucket). 
	 *
	 *                 -------------------- List of bucketlists
	 *                 |       ------------ List of buckets
	 *                 |       |         -- Content of a bucket
	 *                 |       |         |  */
    private static ArrayList<ArrayList<ArrayList<Integer>>> bucketlistHistory = new ArrayList<ArrayList<ArrayList<Integer>>>();
    
    /**
     * This method returns the bucketlist history.
     * 
     * @return bucketlistHistory
     */
	public static ArrayList<ArrayList<ArrayList<Integer>>> getBucketlistHistory(){
		return bucketlistHistory;
	}
	
	/**
	 * Sort a given array using radixsort algorithm
	 * @param list contains the elements to be sorted.
	 * @return the ascending sorted list
     * @throws IllegalArgumentException if list is null.
	 */
	public static ArrayList<Integer> sort(Integer[] list) throws IllegalArgumentException{
		// clear history before each sorting algorithm
		bucketlistHistory.clear();
		int maxNum = findMaxNum(list);
		int digits = getDigits(maxNum);
		int pos = 1;
		int cycle = 0;
		ArrayList<ArrayList<Integer>> history = new ArrayList<>();
		while(digits > 0){
			Integer[] currStep = sortBucket(list,pos,cycle);
			ArrayList<Integer> bucket = new ArrayList<>();
			for( int i = 0; i < currStep.length; i++){
				bucket.add(i,currStep[i]);
			}
			history.add(cycle,bucket);
			digits--;
			pos++;
			cycle++;
		}
		addBucketlistToHistory(history);
		return history.get(history.size()-1);
	}

	private static Integer[] sortBucket(Integer[] list, int pos,int cycle) {

		int range = 10;
		int[] frequency = new int[10];
		for (int i = 0; i < list.length; i++){
			int digit = (list[i] / pos) % range;
			frequency[digit]++;
		}

		for(int j = 1; j < range; j++){
			frequency[j] += frequency[j-1];
		}

		Integer[] sortedValues = new Integer[list.length];
		for(int k = list.length-1; k >= 0; k--){
			int digit = (list[k] / pos) % range;
			sortedValues[frequency[digit] - 1] = list[k];
			frequency[digit]--;
		}
		return sortedValues;
	}

	private static int getDigits(int value) {
		return String.valueOf(value).length();
	}

	private static int findMaxNum(Integer[] list) {
		if(list.length < 1){
			throw new IllegalArgumentException("List is empty");
		}
		int currMax = Integer.MIN_VALUE;
			for (Integer i : list) {
				if (currMax < i) {
					currMax = i;
				}
			}
		return currMax;
	}


	//---------------------------------------------------------------------------------------------------------

	/**
	 * This method creates a snapshot (clone) of the bucketlist and adds it to the bucketlistHistory.
	 * 
	 * @param bucketlist is your current bucketlist, after assigning all elements to be sorted to the buckets.
	 */
	private static void addBucketlistToHistory(ArrayList<ArrayList<Integer>> bucketlist) {
		// clone list!
		ArrayList<ArrayList<Integer>> lstClone = new ArrayList<ArrayList<Integer>>();
		
		for(int i = 0; i<bucketlist.size(); i++) {
			lstClone.add(new ArrayList<Integer>());
			lstClone.get(i).addAll(bucketlist.get(i));
		}
		
		// add clone to the bucketlistHistory
		bucketlistHistory.add(lstClone);
	}
}