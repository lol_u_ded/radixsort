import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;


public class TestAssignment07ex02Student {
	/**
	 * ************************************
	 * Test RadixSort
	 * ************************************
	 */
	Integer testArray[] = {111,32,4,77};
	
	@Test
	public void testRadixSortResult() {
		
		ArrayList<Integer> res = RadixSort.sort(testArray.clone());

		// test result
		System.out.print("ArrayList: " + res);
		assertEquals(Integer.valueOf(4), res.get(0));
		assertEquals(Integer.valueOf(32), res.get(1));
		assertEquals(Integer.valueOf(77), res.get(2));
		assertEquals(Integer.valueOf(111), res.get(3));
	}
	
	@Test
	public void testRadixSortBucketlists() {
		ArrayList<Integer> res = RadixSort.sort(testArray.clone());
				
		ArrayList<ArrayList<ArrayList<Integer>>> bucketlistHistory = RadixSort.getBucketlistHistory();
		//printBucketlists(bucketlistHistory);
		
			
		// test intermediate steps
		// Iteration 1:
		assertTrue(bucketlistHistory.get(0).get(0).size() == 0);	// bucket 0 must be empty
		assertTrue(bucketlistHistory.get(0).get(1).size() == 1); 	// bucket 1 must contain 111 only
		assertTrue(bucketlistHistory.get(0).get(1).get(0) == Integer.valueOf(111));
		assertTrue(bucketlistHistory.get(0).get(2).size() == 1); 	// bucket 2 must contain 32 only
		assertTrue(bucketlistHistory.get(0).get(2).get(0) == Integer.valueOf(32));
		assertTrue(bucketlistHistory.get(0).get(3).size() == 0);	// bucket 3 must be empty
		assertTrue(bucketlistHistory.get(0).get(4).get(0) == Integer.valueOf(4));
		assertTrue(bucketlistHistory.get(0).get(5).size() == 0);	// bucket 5 must be empty
		assertTrue(bucketlistHistory.get(0).get(6).size() == 0);	// bucket 6 must be empty
		assertTrue(bucketlistHistory.get(0).get(7).get(0) == Integer.valueOf(77));
		assertTrue(bucketlistHistory.get(0).get(8).size() == 0);	// bucket 8 must be empty
		assertTrue(bucketlistHistory.get(0).get(9).size() == 0);	// bucket 9 must be empty
		
		// Todo: Iteration 2..x:
		// ...
	}
	@Test
	public void testFindMaxNum(){

		Integer[] list = {0,121,5,38,99,358,4,19};
		assertEquals(358,findMaxNum(list));
		Integer[] listEmpty = {};
		assertEquals(Integer.MIN_VALUE,findMaxNum(listEmpty));
	}

	
	/**
	 * This method may support you in debugging by printing the bucketlistHistory formated.
	 * 
	 * @param list should contain the bucketlistHistory after sort()-method has been called.
	 */
	private void printBucketlists(ArrayList<ArrayList<ArrayList<Integer>>> list) {
		for(int i = 0; i < list.size(); i++) {
			System.out.println("\nIteration #"+(i+1));
			
			ArrayList<ArrayList<Integer>> tmp = list.get(i);
			
			
			// get size of largest bucket
			int maxSize = 0;
			for(int j = 0; j < tmp.size(); j++) {
				if(tmp.get(j).size() > maxSize) maxSize = tmp.get(j).size();
			}
			
			// print header
			System.out.print("Bucket:");
			for(int j = 0; j< tmp.size();j++) {
				System.out.printf("\t%d",j);
			}
			System.out.print("\n----------------------------------------------------------------------------------------\n");
			
			for(int j = 0; j < maxSize; j++) {
				
				// print line by line content of all buckets (first entries of all buckets, then second entries,...)
				for(int k = 0; k < tmp.size(); k++) {
					if(j < tmp.get(k).size())
						System.out.print("\t"+tmp.get(k).get(j));
					else System.out.print("\t");
				}
				System.out.println();
			}
		}
	}
	// to be tested
	private static int findMaxNum(Integer[] list) {
		int currMax = Integer.MIN_VALUE;
		if(list.length < 1){
			return currMax;
		}
			for (Integer i : list) {
				if (currMax < i) {
					currMax = i;
				}
			}
		return currMax;
	}
}
